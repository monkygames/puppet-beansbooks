## 2016-04-25 Release 0.1.2
### Summary
Fixed future parser errors

### Changes
  - Fixed issues with future parsing

## 2016-04-25 Release 0.1.1
### Summary
Initial Release

### Changes
  - Migrated to bitbucket
  - Completed implementation
