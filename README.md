# beansbooks

[![Puppet Forge](http://img.shields.io/puppetforge/v/monkygames/beansbooks.svg)](https://forge.puppetlabs.com/monkygames/beansbooks)

## Overview

Installs and configures [System 76's Beansbooks](https://github.com/system76/beansbooks) cloud accounting platform.

## Module Description

Installs and configures apache & postgresql from the puppetforge.
Downloads the source for System76's Beansbooks (php) and sets up web site.


## Usage

The most basic usage is the following:

```
class {'beansbooks':
  sha_hash             => 'hash',
  sha_salt             => 'salt',
  cookie_salt          => 'cookie_salt',
  key                  => 'key',
  admin_user_full_name => 'admin',
  admin_user_email     => 'admin@admin.com',
  admin_user_pass      => 'admin',
}
```


###Class: beansbooks

The main class for deploying beansbooks.  This is the only necessary class to declare.

#### Required Parameters
##### `sha_hash`
A strong key defined here: [beansbooks/application/classes/beans/example.config.php](https://github.com/system76/beansbooks/blob/master/application/classes/beans/example.config.php)

##### `sha_salt`
A strong key defined here: [beansbooks/application/classes/beans/example.config.php](https://github.com/system76/beansbooks/blob/master/application/classes/beans/example.config.php)

##### `cookie_salt`
A strong key defined here: [beansbooks/application/classes/beans/example.config.php](https://github.com/system76/beansbooks/blob/master/application/classes/beans/example.config.php)

##### `key`
A strong key defined here: [beansbooks/application/classes/beans/example.config.php](https://github.com/system76/beansbooks/blob/master/application/classes/beans/example.config.php)

##### `admin_user_full_name`
The full name of the admin or owner of the site.

##### `admin_user_email`
The admin's email address.

##### `admin_user_pass`
The password of the admin user (to login to the web site.

#### Optional Parameters

##### `servername`
The FQDN or hostname of the server.  
Default: 'localhost'

##### `db_host`
The name or ip of the host for the database.  
Default: 'localhost'

##### `db_user`
The database user name for accessing beansbooks data.  
Default: 'beans'

##### `db_pass`
The password for accessing the database.  
Default: 'beans'

##### `db_name`
The name of the database to store the beansbooks data.  
Default: 'beansdb'

##### `db_root_pass`
The password for the root database user.  
Default 'beans'

##### `version`
The beansbooks release version (label on github).  
Default: 'v1.5.2'

##### `timezone`
A string that represents the php timezone.  
Valid values: http://php.net/manual/en/timezones.php
Default: 'GMT'  

##### `ssl_cert`
The path to the ssl certificate file to be set in the vhost.  
Default: '/etc/ssl/certs/ssl-cert-snakeoil.pem'

##### `ssl_key`
The path to the key to the certificate file to be set in the vhost.  
Default: '/etc/ssl/private/ssl-cert-snakeoil.key'

##### `ssl_chain`
The path to the ssl chain file (rootca) in the vhost.  
Default: undef

## Testing
This module has been tested on the following beansbooks versions:
* v1.5.1
* v1.5.2

### Vagrant Environment
This module uses vagrant and puppet for testing.  The vagrant directory contains the testing configuration.

#### Setup
Install the following programs:
* virtualbox
* vagrant
* puppet

To run a test, do the following:
* cd to the vagrant directory
* run the run.bash script
* use a browser to login to the site at 192.168.33.12

## Limitations

Only works with debian based OS's.

## Development

The module is open source and available on bitbucket.  Please fork!