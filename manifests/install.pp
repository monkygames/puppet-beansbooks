# == Class: beansbooks::install
#
# Installs the required packages.
#
# === Parameters
#
# === Variables
#
#
# === Examples
#
#
# === Copyright
#
# GPL-3.0+
#
class beansbooks::install {

  anchor {'beansbooks::install::begin':}

  ## packages
  $packages = ['git']

  $php_packages = {
      gd     => {},
      curl   => {},
      mysql  => {},
      mcrypt => {},
      json   => {},
  }

  package{$packages:
    ensure  => installed,
    require => Anchor['beansbooks::install::begin'],
  }

  class {'php':
    dev        => false,
    composer   => false,
    fpm        => false,
    pear       => true,
    phpunit    => false,
    extensions => $php_packages,
  }

  php::config::setting {'Data/date.timezone':
    key     => 'Data/date.timezone',
    value   => $beansbooks::timezone,
    file    => '/etc/php5/apache2/php.ini',
    require => Class['php'],
  }

  ## apache
  class {'apache':
    mpm_module    => 'prefork',
    default_vhost => false,
    servername    => $beansbooks::servername,
    require       => Package[$packages],
  }
  class {'apache::mod::php': }
  class {'apache::mod::rewrite': }
  class {'apache::mod::ssl': }

  apache::vhost { 'beansbooks_non-ssl':
    servername    => $beansbooks::servername,
    port          => '80',
    default_vhost => false,
    docroot       => $beansbooks::droot,
    rewrites      => [
      {
        comment      => 'redirect to https',
        rewrite_cond => ['%{HTTPS} off'],
        rewrite_rule => ['(.*) https://%{HTTP_HOST}:443%{REQUEST_URI}'],
      },
    ],
    require       => Anchor['beansbooks::install::begin'],
  }

  ## configure apache
  apache::vhost { 'beansbooks-ssl':
    servername      => $beansbooks::servername,
    port            => '443',
    docroot         => $beansbooks::droot,
    default_vhost   => true,
    ssl             => true,
    ssl_cert        => $beansbooks::ssl_cert,
    ssl_key         => $beansbooks::ssl_key,
    ssl_chain       => $beansbooks::ssl_chain,
    directories     => [
      {
        path           => $beansbooks::droot,
        auth_require   => 'all granted',
        allow_override => ['All'],
        options        => ['FollowSymLinks'],
      },
      {
        path            => '\.(cgi|shtml|phtml|php)$',
        provider        => 'filesmatch',
        custom_fragment => 'SSLOptions +StdEnvVars',
      }
    ],
    custom_fragment =>
  "BrowserMatch \"MSIE [2-6]\" nokeepalive ssl-unclean-shutdown downgrade-1.0\
 force-response-1.0\n BrowserMatch \"MSIE [17-9]\" ssl-unclean-shutdown",
    require         => Anchor['beansbooks::install::begin'],
  }

  ## source code
  vcsrepo { $beansbooks::droot:
    ensure   => present,
    provider => 'git',
    source   => 'https://github.com/system76/beansbooks.git',
    revision => $beansbooks::version,
    require  => Class['apache','::apache::mod::php','::apache::mod::rewrite']
  }

  anchor {'beansbooks::install::end':
    require => Vcsrepo[$beansbooks::droot],
  }
}
