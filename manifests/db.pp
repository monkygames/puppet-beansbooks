# == Class: beansbooks::db
#
# Installs the required packages.
#
# === Parameters
#
# === Variables
#
#
# === Examples
#
#
# === Copyright
#
# GPL-3.0+
#
class beansbooks::db {
  anchor{'beansbooks::db::begin':}

  class {'mysql::server':
    root_password           => $beansbooks::db_root_pass,
    remove_default_accounts => true,
    require                 => Anchor['beansbooks::db::begin'],
  }

  class {'mysql::client':
    require => Class['mysql::server'],
  }

  class {'mysql::bindings':
    python_enable => true,
  }

  mysql::db { $beansbooks::db_name:
    user     => $beansbooks::db_user,
    password => $beansbooks::db_pass,
    host     => $beansbooks::db_host,
    charset  => 'utf8',
    grant    => ['ALL'],
  }

  anchor{'beansbooks::db::end':
    require => Class['mysql::server'],
  }
}
