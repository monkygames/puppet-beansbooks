# == Class: beansbooks
#
# Installs and configures the beansbooks web site.
#
# === Parameters
#
# [*sha_hash*]
#   A strong key defined here:
#   beansbooks/application/classes/beans/example.config.php
#
# [*sha_salt*]
#   A strong key defined here:
#   beansbooks/application/classes/beans/example.config.php
#
# [*cookie_salt*]
#   A strong key defined here:
#   beansbooks/application/classes/beans/example.config.php
#
# [*key*]
#   A strong key defined here:
#   beansbooks/application/classes/beans/example.config.php
#
# [*admin_user_full_name*]
#   The full name of the admin or owner of the site.
#
# [*admin_user_email*]
#   The admin's email address.
#
# [*admin_user_pass*]
#   The password of the admin user (to login to the web site.
#
# [*servername*]
#   The FQDN or hostname of the server.
#   Default: 'localhost'
#
# [*db_host*]
#   The name or ip of the host for the database.
#   Default: 'localhost'
#
# [*db_user*]
#   The database user name for accessing beansbooks data.
#   Default: 'beans'
#
# [*db_pass*]
#   The password for accessing the database.
#   Default: 'beans'
#
# [*db_name*]
#   The name of the database to store the beansbooks data.
#   Default: 'beansdb'
#
# [*db_root_pass*]
#   The password for the root database user.
#   Default 'beans'
#
# [*version*]
#   The beansbooks release version (label on github).
#   Default: 'v1.5.2'
#
# [*timezone*]
#   A string that represents the php timezone.
#   Valid values: http://php.net/manual/en/timezones.php
#   Default: 'GMT'
#
# [*ssl_cert*]
#   The path to the ssl certificate file to be set in the vhost.
#   Default: '/etc/ssl/certs/ssl-cert-snakeoil.pem'
#
# [*ssl_key*]
#   The path to the key to the certificate file to be set in the vhost.
#   Default: '/etc/ssl/private/ssl-cert-snakeoil.key'
#
# [*ssl_chain*]
#   The path to the ssl chain file (rootca) in the vhost.
#   Default: undef
#
# === Variables
#
#
# === Examples
#
#
# === Copyright
#
# GPL-3.0+
#
class beansbooks (
  $sha_hash,
  $sha_salt,
  $cookie_salt,
  $key,
  $admin_user_full_name,
  $admin_user_email,
  $admin_user_pass,
  $servername   = 'localhost',
  $db_host      = 'localhost',
  $db_user      = 'beans',
  $db_pass      = 'beans',
  $db_name      = 'beansdb',
  $db_root_pass = 'beans',
  $version      = 'v1.5.2',
  $timezone     = 'GMT',
  $ssl_cert     = '/etc/ssl/certs/ssl-cert-snakeoil.pem',
  $ssl_key      = '/etc/ssl/private/ssl-cert-snakeoil.key',
  $ssl_chain    = undef,
){

  ## variables
  $droot = '/opt/beansbooks'
  anchor {'beansbooks::begin':}

  class {'beansbooks::db':
    require => Anchor['beansbooks::begin']
  }
  class {'beansbooks::install':
    require => Class['beansbooks::db'],
  }
  class {'beansbooks::config':
    require => Class['beansbooks::install']
  }

  anchor {'beansbooks::end':}

}
