# == Class: beansbooks::db
#
# Installs the required packages.
#
# === Parameters
#
# === Variables
#
#
# === Examples
#
#
# === Copyright
#
# GPL-3.0+
#
class beansbooks::params {
  $mysql_packages = [ 'php5-mysql', 'mysql-client']
  $psql_packages = ['php5-pgsql']

}
