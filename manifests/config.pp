# == Class: beansbooks::config
#
#
# === Parameters
#
# === Variables
#
#
# === Examples
#
#
# === Copyright
#
# GPL-3.0+
#
class beansbooks::config {

  anchor{'beansbooks::config::begin':}

  # add a host entry
  host { 'beansbooks':
    ip => '127.0.0.1',
  }

  file {"${beansbooks::droot}/application/logs":
    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0770',
    recurse => true,
    require => Anchor['beansbooks::config::begin'],
  }
  file {"${beansbooks::droot}/application/cache":
    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0770',
    recurse => true,
    require => File["${beansbooks::droot}/application/logs"],
  }

  file { "${beansbooks::droot}/application/classes/beans/config.php":
    ensure  => file,
    mode    => '0660',
    owner   => 'www-data',
    content => template('beansbooks/config.php.erb'),
    require => File["${beansbooks::droot}/application/cache"],
  }

  file { "${beansbooks::droot}/.htaccess":
    ensure  => file,
    mode    => '0660',
    owner   => 'www-data',
    source  => "file:///${beansbooks::droot}/example.htaccess",
    require => File["${beansbooks::droot}/application/classes/beans/config.php"]
  }


  check_run::task {'web_install':
    exec_command => "/usr/bin/php index.php --uri=/install/manual\
 --name='${beansbooks::admin_user_full_name}'\
 --password='${beansbooks::admin_user_pass}'\
 --email='${beansbooks::admin_user_email}'\
 --accounts='full'",
    cwd          => $beansbooks::droot,
    require      => File["${beansbooks::droot}/.htaccess"],
  }

  anchor{'beansbooks::config::end':
    require => Check_run::Task['web_install'],
  }
}